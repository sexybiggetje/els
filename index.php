<?php
$dbname = "els.txt";
$notfound = "notfound.html";
$error = "error.html";

if ( !file_exists( $dbname ) ) {
	header( $_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500 );
	echo file_get_contents( $error );
	exit;
}

if ( !isset( $_GET[ 'redir' ] ) ) {
	header( $_SERVER['SERVER_PROTOCOL'] . ' 404 Page not found', true, 500 );
	echo file_get_contents( $notfound );
	exit;
}

$dbc = file_get_contents( $dbname );
foreach ( explode( "\n", $dbc ) as $line ) {
	if ( substr( $line, 0, 1 ) === '#' ) {
		continue;
	}

	$parts = explode( ' ', trim( $line ) );
	if ( count( $parts ) === 2 && $parts[ 0 ] === $_GET[ 'redir' ] ) {
		header( 'Location: ' . $parts[ 1 ] );
		exit;
	}
}

header( $_SERVER['SERVER_PROTOCOL'] . ' 404 Page not found', true, 404 );
echo file_get_contents( $notfound );
exit;
